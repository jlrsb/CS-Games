// Styles
import '@mdi/font/css/materialdesignicons.css'
import 'vuetify/styles'

import { VDataTable } from 'vuetify/labs/VDataTable'

// Composables
import { createVuetify } from 'vuetify'

export default createVuetify({
  components: {VDataTable},
  theme: {
    themes: {
      light: {
        colors: {
          primary: '#3a8ab9',
          secondary: '#5CBBF6',
        },
      },
    },
  },
})
