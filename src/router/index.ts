// Composables
import { createRouter, createWebHistory } from 'vue-router'

const routes = [
  {
    path: '/',
    component: () => import('@/layouts/default/Default.vue'),
    children: [
      {
        path: '',
        name: 'Home',
        component: () => import(/* webpackChunkName: "home" */ '@/views/Home.vue'),
      },
      {
        path: '',
        name: 'Puzzle',
        component: () => import(/* webpackChunkName: "home" */ '@/views/Puzzle.vue'),
      },
      {
        path: '',
        name: 'Login',
        component: () => import(/* webpackChunkName: "home" */ '@/views/Login.vue'),
      },
      {
        path: '',
        name: 'Teams',
        component: () => import(/* webpackChunkName: "home" */ '@/views/Teams.vue'),
      },
      {
        path: '',
        name: 'Leaderboard',
        component: () => import(/* webpackChunkName: "home" */ '@/views/Leaderboard.vue'),
      },
      {
        path: ':name',
        name: 'Flags',
        component: () => import(/* webpackChunkName: "home" */ '@/views/Flags.vue'),
      },    
      {
        path: '',
        name: 'unknownError',
        component: () => import(/* webpackChunkName: "home" */ '@/views/unknownError.vue'),
      },
    ],
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
})

export default router
