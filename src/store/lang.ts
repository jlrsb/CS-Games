import { defineStore } from 'pinia'
import { ref } from 'vue'

export const useLangStore = defineStore('lang', {
    state:() => ({langIsFrench : true}),

    actions:{
        updateLang(){
            this.langIsFrench = !this.langIsFrench
        }
    }
})